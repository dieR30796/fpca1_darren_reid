-- Darren Reid's CA 1 for Functional Programming, John Loane --
{-The test function with part of each question is a generic outline 
of what to use you can replace those numbers or characters and 
test the code also-}

-- Question 1(Tested: Yes. Adds two ints and doubles that value.)
--Test Function with: add_and_double 4 5 Output is: 18
add_and_double :: Int -> Int -> Int--
add_and_double x y = (x + y) * 2

-- Question 2(Tested: Yes. Same as above except its now part of the infix function +*) --
--Test Function with: testInfix 4 5 Output is: 18
(+*) :: Int -> Int -> Int
(+*) a b = a `add_and_double` b  --Setting up infix.
testInfix a b = a +* b             --Showing infix working.

-- Question 3(Tested: Yes. Solves quadratic equations.) --
--Test Function with: solve_quadratic_equation 1.0 2.0 1.0 Output is: (-1.0,-1.0)
solve_quadratic_equation :: Double -> Double -> Double -> (Double,Double)
solve_quadratic_equation a b c = (x,y)--x and y are the two doubles in our tuple outputted.
   where 
      x = e + sqrt d / (2 * a)--Working out the values of x and y.
      y = e - sqrt d / (2 * a)
      d = b * b - 4 * a * c
      e = - b / (2 * a)					

-- Question 4(Tested: Yes. Gets a list of numbers from 1 to x.) --
--Test Function with: first_n 5 Output is: [1,2,3,4,5]
first_n :: Int -> [Int]
first_n n = take n [1..n]
-- Question 5(Tested: ) --

-- Question 6(Tested: Yes) --
--Test Function with: double_factorial 5 Output is: [1,1,2,6,24,120]
double_factorial :: Integer -> [Integer]
double_factorial 0 = [1]
double_factorial x = double_factorial (x - 1) ++ [factorial x] --Recursion of double_factorial
   where
      factorial 0 = 1                     --Factorial of 0 is 1 so thats why we set it as 1 here.
      factorial y = y * factorial (y - 1) --recursion of factorial.

-- Question 7(Tested: ) --


-- Question 8(Tested: Yes. Checks if a number is a prime number.) --
--Test Function with: isPrime 5 Output is: True
isPrime :: Integer -> Bool
isPrime n = length[x| x <- [2..n], n `mod` x == 0] == 1 


-- Question 9(Tested: Yes. List prime numbers up to a number inputted.) --
--Test Function with: primes 5 Output is: [2,3,5,7,11]
primes p = listP	
   where 
   listP = take p primeList 
   primeList = [x| x <- [1..], isPrime x] 


-- Question 10(Tested: Yes. Sums a list of integers recursively.) --
--Test Function with: recursiveSum' [1..5] Output is: 15
recursiveSum' :: [Int] -> Int	
recursiveSum' [x] = x
recursiveSum' (x:xs) = x + recursiveSum' xs

-- Question 11(Tested: Yes. Same as q10 just using foldl.) --
--Test Function with: foldl' [1..5] Output is: 15
foldl' :: [Int] -> Int
foldl' = foldl (+) 0 

-- Question 12(Tested: ) --
--Test Function with: Output is:


-- Question 13(Tested: Yes) --
--Test Function with: guess "I love functional programming" 4  Output is: "You Win Lad"
guess :: (Ord a , Num a) => [Char] -> a -> [Char]
guess x y
   |x == str && y < 5 = "You Win Lad"
   |x /= str && y < 5 = "Guess Again brotha"
   |otherwise = "You Lost Fam"
   where str = "I love functional programming"   


-- Question 14(Tested: ) --
--Test Function with: Output is:

-- Question 15(Tested: Yes. Checks if number is even or not.) --
--Test Function with: is_even 5 Output is: False
is_even :: Integral a => a -> Bool
is_even n = if   n `mod` 2 == 0
   then True
   else False

-- Question 16(Tested: Yes. Takes vowels out of strings.) --
--Test Function with: unixname "the house" Output is: "th hs"
unixname :: [Char] -> [Char]
unixname [] = []
unixname xs = [x | x <- xs, x `notElem` "aeiouAEIOU"]

-- Question 17(Tested: Yes. Compares to lists and sees if they share an element with each other, outputs this element.) --
--Test Function with: listIntersect [1..5] [2..6] Output is: [2,3,4,5]
listIntersect :: [Integer] -> [Integer]  -> [Integer]
listIntersect xs ys = [x | x <- xs, x `elem` ys]

-- Question 18(Tested: Yes. Censors any vowels and replaces them with X.) --
--Test Function with: Output is:
censor :: [Char] -> [Char]
censor [] = []
censor (x:xs)
   |x `elem` "aeiouAEIOU" = 'X' : censor xs
   |otherwise = x : censor xs
   